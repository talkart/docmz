'use strict';
define('app', ['angular'], function (angular) {
    var app = angular.module('app', ['ngRoute', 'ui.router', 'ui.tree']);

    app.factory('UserLoginCheckInterceptor', ["$q", "$rootScope", '$location', 'UserService', function ($q, $rootScope, $location, userService) {
        return {
            response: function (response) {
                if (('data' in response) && (typeof response.data == 'object')) {
                    if ('status' in response.data) {
                        if (response.data.status == 0) {
                            if (response.data.info == 'login_required') {
                                $location.path('/login');
                            } else if (response.data.info == 'login_success') {
                                userService.user.logined = true;
                                userService.user.username = response.data.username;
                                $location.path('/doc');
                            }
                        }
                    }
                }
                return response;
            }
        };
    }]);

    app.config(['$httpProvider', function ($httpProvider) {
        $httpProvider.defaults.headers.post["Content-Type"] =
            "application/x-www-form-urlencoded";
        $httpProvider.defaults.
            transformRequest.unshift(function (data,
                                               headersGetter) {
                var key, result = [];
                for (key in data) {
                    if (data.hasOwnProperty(key)) {
                        result.push(encodeURIComponent(key) + "="
                        + encodeURIComponent(data[key]));
                    }
                }
                return result.join("&");
            });
    }]);

    app.directive('ngFocus', [function () {
        var FOCUS_CLASS = "ng-focused";
        return {
            restrict: 'A',
            require: 'ngModel',
            link: function (scope, element, attrs, ctrl) {
                ctrl.$focused = false;
                element.bind('focus', function (evt) {
                    element.addClass(FOCUS_CLASS);
                    scope.$apply(function () {
                        ctrl.$focused = true;
                    });
                }).bind('blur', function (evt) {
                    element.removeClass(FOCUS_CLASS);
                    scope.$apply(function () {
                        ctrl.$focused = false;
                    });
                });
            }
        }
    }]);

    app.directive('ngEqual', [function () {
        return {
            require: 'ngModel',
            link: function (scope, elem, attrs, ctrl) {
                var target = '#' + attrs.ngEqual;
                elem.add(target).on('keyup', function () {
                    scope.$apply(function () {
                        var v = elem.val() === $(target).val();
                        ctrl.$setValidity('equal', v);
                    });
                });
            }
        }
    }]);

    /*
     app.directive('ngContextMenu', [function () {
     return function (scope, element, attrs, ctrl) {
     if (scope.$last) {

     $.contextMenu({
     selector: '.doc-item',
     callback: function (key, options) {
     var m = "clicked: " + key;
     window.console && console.log(m) || alert(m);
     },
     items: {
     "edit": {name: "编辑"},
     "delete": {name: "删除"}
     //"sep": "---------"
     }
     });
     $(document.body).on("contextmenu:focus", ".doc-item", function (e) {
     console.log("focus:", this);
     });

     }
     };
     }]);
     */

    app.directive('ngShowOnHoverParent', function () {
        return {
            link: function (scope, element, attrs) {
                var p = attrs.ngShowOnHoverParent;
                var pDom = element.parent(p);
                pDom.bind('mouseenter', function () {
                    $(element).show();
                });
                pDom.bind('mouseleave', function () {
                    $(element).hide();
                });
            }
        };
    });

    app.directive('a', function () {
        return {
            restrict: 'E',
            link: function (scope, elem, attrs) {
                if (attrs.ngClick || attrs.href === '' || attrs.href === '#') {
                    elem.on('click', function (e) {
                        if ($(this).hasClass('flag-stop-propagation')) {
                            e.preventDefault();
                            e.stopPropagation();
                        }
                    });
                }
            }
        };
    });

    app.config(['$httpProvider', function ($httpProvider) {
        $httpProvider.interceptors.push('UserLoginCheckInterceptor');
    }]);

    app.filter('trustHtml', ['$sce', function ($sce) {
        return function (input) {
            return $sce.trustAsHtml(input);
        }
    }]);


    return app;
});