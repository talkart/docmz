<?php

namespace Admin\Controller;

class CmsMemberExtraInfoController extends CmsController
{
    // 导出菜单
    // cmslist、cmshandle为必须要到导出的字段
    static $export_menu = array(
        'user' => array(
            '用户配置' => array(
                'cmslist' => array(
                    'title' => '用户配置列表',
                    'hiddens' => array(
                        'cmshandle' => '用户配置管理',
                    )
                ),
            )
        )
    );
    // 标识字段，该字段为自增长字段
    public $cms_pk = 'id';
    // 数据表名称
    public $cms_table = 'member_extra_info';
    // 数据库引擎
    public $cms_db_engine = 'MyISAM';
    // 列表列出的列出字段
    public $cms_fields_list = array(
        'id',
        'uid',
        'doc_header_image'
    );
    // 添加字段，留空表示所有字节均为添加项
    public $cms_fields_add = array();
    // 编辑字段，留空表示所有字节均为编辑项
    public $cms_fields_edit = array();
    // 搜索字段，表示列表搜索字段
    public $cms_fields_search = array();
    // 数据表字段
    public $cms_fields = array(

        'uid' => array(
            'title' => '用户ID',
            'description' => '',
            'type' => 'member_uid',
            'default' => '0',
            'rules' => 'required|searchable'
        ),

        'doc_header_image' => array(
            'title' => '本地图片',
            'description' => '说明',
            'type' => 'imagefile',
            'default' => '',
            'extension' => array(
                'jpg',
                'png'
            ),
            'rules' => 'required'
        ),

    );
}
