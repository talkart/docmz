<?php

namespace Admin\Controller;

class ModMemberUploadController extends ModController
{
    static $export_menu = array(
        'user' => array(
            '用户上传' => array(
                'filelist' => array(
                    'title' => '文件管理',
                    'hiddens' => array()
                ),
                'config' => array(
                    'title' => '上传配置',
                    'hiddens' => array()
                ),
            )
        )
    );

    public function build($yummy = false)
    {
        parent::build($yummy);

        switch ($this->build_db_type) {
            case 'mysql' :

                $sqls = array();

                if (!$this->build_table_exists("member_upload")) {

                    $table_name = $this->build_db_prefix . "member_upload";
                    $sqls [] = "DROP TABLE IF EXISTS `$table_name`";
                    $sqls [] = "
								CREATE TABLE `$table_name` (
								
									`id` INT UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID',
									`uid` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '用户ID',
									`data_id` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '文件ID，对应data_files中的ID',

									PRIMARY KEY (`id`),
									KEY `uid`(`uid`)
								
								) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
								";

                }

                if (!$this->build_table_exists("member_upload_space")) {

                    $table_name = $this->build_db_prefix . "member_upload_space";
                    $sqls [] = "DROP TABLE IF EXISTS `$table_name`";
                    $sqls [] = "
								CREATE TABLE `$table_name` (

									`id` INT UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID',
									`uid` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '用户ID',
									`space` BIGINT UNSIGNED NOT NULL DEFAULT 0 COMMENT '最大空间数',

									PRIMARY KEY (`id`),
									KEY `uid`(`uid`)

								) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
								";

                }

                break;
        }

        foreach ($sqls as $sql) {
            $this->build_db->execute($sql);
        }

        parent::build_success($yummy);
    }

    public function filelist()
    {
        if (IS_POST) {

            $current = I('post.current', 1, 'intval');
            $rowCount = I('post.rowCount', 10, 'intval');
            $sort = I('post.sort');
            $searchPhrase = I('post.searchPhrase');

            $m = D('MemberUpload');

            // pre process
            $where = array();
            if ($searchPhrase) {
                $where ['mu.uid'] = array(
                    'LIKE',
                    "%$searchPhrase%"
                );
                $where ['mu.id'] = array(
                    'LIKE',
                    "%$searchPhrase%"
                );
            }
            $order = null;
            foreach ($sort as $f => $d) {
                $order = "mu.$f $d";
            }
            if (empty ($order)) {
                $order = 'mu.id ASC';
            }

            // get info
            $total = $m->table('__MEMBER_UPLOAD__ mu')->where($where)->count();
            if ($order) {
                $m->order($order);
            }
            $data = array();
            $datas = $m->table('__MEMBER_UPLOAD__ mu')->field('mu.id,mu.uid,mu.data_id,df.filesize,df.dir,df.path')->join('__DATA_FILES__ df ON mu.data_id=df.id')->where($where)->page($current, $rowCount)->select();
            if (!empty ($datas)) {
                foreach ($datas as &$v) {
                    $data [] = array(
                        'id' => $v['id'],
                        'uid' => $v['uid'],
                        'data_id' => $v['data_id'],
                        'filepath' => $v['dir'] . '/' . $v['path'],
                        'filesize' => byte_format($v['filesize'])
                    );
                }
            }

            $json = array(
                'current' => $current,
                'rowCount' => $total > $rowCount ? $rowCount : $total,
                'total' => $total,
                'rows' => $data
            );
            $this->ajaxReturn($json);
        }
        $this->display();
    }

    public function filehandle($action = '', $id = 0)
    {
        $id = intval($id);
        $m = D('MemberUpload');
        switch ($action) {
            case 'delete' :
                $ids = array();
                foreach (explode(',', I('post.ids', '', 'trim')) as $id) {
                    $id = intval($id);
                    if ($id) {
                        $ids [] = $id;
                    }
                }
                $m->delete(join(',', $ids));
                $this->success('OK');
                break;
        }
    }

    public function config()
    {
        if (IS_POST) {
            tpx_config('member_upload_space', formated_size_to_bytes(I('post.member_upload_space', '', 'trim')));
            $this->success('保存成功');
        }

        $this->data_member_upload_space = byte_format(tpx_config_get('member_upload_space', 0));

        $this->display('ModMemberUpload:config');
    }

}