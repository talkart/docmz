<?php
/*
 * 根据邮箱，密码，验证码注册
 * 用于在 /User/login 页面
 *
 * 需要预先设定的值
 *      $title [可选]
 *      $redirect [可选]
 */
if (!defined('THINK_PATH')) {
    exit();
}


if (IS_POST) {

    $username = I('post.username', '', 'trim');
    $password = I('post.password');

    $msg = $this->_am->login($email = '', $cellphone = '', $username, $password, $ret_uid);
    if (true === $msg) {
        $_SESSION ['member_user_uid'] = $ret_uid;

        $redirect = I('session.login_redirect', U('User/profile'));
        if (isset($_SESSION['login_redirect'])) {
            unset($_SESSION['login_redirect']);
        }
        $this->success('', $redirect);
    } else {
        $this->error($msg);
    }
}


if (empty($redirect) && empty($_SESSION['login_redirect'])) {
    $redirect = I('server.HTTP_REFERER');
}

if (!empty($redirect)) {
    $_SESSION['login_redirect'] = $redirect;
}

if (!empty($_SESSION['member_user_uid'])) {
    $redirect = I('session.login_redirect', U('User/profile'));
    if (isset($_SESSION['login_redirect'])) {
        unset($_SESSION['login_redirect']);
    }
    header('Location: ' . $redirect);
    exit();
}

if (empty($title)) {
    $title = tpx_config_get('home_title', '');
}
$this->assign('page_title', '用户登录 - ' . $title);
$this->assign('page_keywords', '用户登录,' . $title . '登录');
$this->assign('page_description', '登录' . $title);
$this->display();