<?php

namespace Home\Controller;

use Think\Controller;
use Think\Log;

class WatcherController extends Controller
{
    public function index()
    {

    }

    public function auto_deploy()
    {
        shell_exec("git pull origin master");
        shell_exec("git checkout -f _CFG/upgrade.lock");
        if (file_exists($file = './_RUN/common~runtime.php')) {
            @unlink($file);
        }
        echo 'SUCCESS';
    }

    public function js()
    {
        $file = I('get.file', '', 'trim');
        $line = I('get.line', '', 'trim');
        $error = I('get.error', '', 'trim');
        $url = I('get.url', '', 'trim');
        $agent = I('get.agent', '', 'trim');

        $_SERVER['REQUEST_URI'] = __ROOT__ . '/watcher/js';
        Log::record("Url=$url, File=$file, Line=$line, Error=$error, Agent=$agent");

    }


}
