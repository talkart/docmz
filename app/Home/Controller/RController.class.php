<?php

namespace Home\Controller;

use Org\Net\Http;
use Think\Controller;

class RController extends Controller
{

    public function download($out_id = '')
    {
        $file = @file_get_contents('http://' . HTTP_HOST . U('R/' . $out_id) . ':download');

        if (empty($out_id)) {
            $this->error('错误的链接');
        }

        if (empty($file)) {
            $this->error('错误的数据');
        }

        $one = D('CmsDoc')->where(array('out_id' => $out_id))->find();
        if (empty($one)) {
            $this->error('文档不存在');
        }

        Http::download('', $one['title'] . '.html', $file);
    }

    public function _empty($out_id_with_type = '')
    {
        $type_list = array(
            'download' => 'download',
            'pdf' => 'pdf',
            'preview' => 'preview',
            'mobile' => 'mobile'
        );
        list($out_id, $type) = explode(':', $out_id_with_type);
        if (!isset($type) || !isset($type_list[$type])) {
            $type = 'preview';
        }

        if (empty($out_id)) {
            $this->error('错误的链接');
        }

        $one = D('CmsDoc')->where(array('out_id' => $out_id))->find();
        if (empty($one)) {
            $this->error('文档不存在');
        }

        $oned = D('CmsDocData')->where(array('doc_id' => $one['id']))->find();
        if (empty($one)) {
            $this->error('文档正文找不到了 T_T');
        }

        $one['content'] = $oned['content'];

        switch ($type) {
            case 'html':
                preg_match_all('/!\\[(.*?)\\]\\((.*?)\\)/', $one ['content'], $mats);
                if (!empty($mats[2])) {
                    $filter = array();
                    foreach ($mats[2] as $k => $url) {
                        $host = preg_quote(HTTP_HOST);
                        if (preg_match('/^http:\\/\\/' . $host . '/i', $url)) {
                            if (!isset($filter[$url])) {
                                $filter[$url] = true;
                                $img = file_get_contents($url);
                                if ($img) {
                                    $data_type = 'image/jpg';
                                    if (preg_match('/\\.png$/i', $url)) {
                                        $data_type = 'image/png';
                                    } else if (preg_match('/\\.gif/i', $url)) {
                                        $data_type = 'image/gif';
                                    } else if (preg_match('/\\.svg/i', $url)) {
                                        $data_type = 'image/svg';
                                    }
                                    $one ['content'] = str_replace($mats[0][$k], "![" . $mats[1][$k] . "](data:$data_type;base64," . base64_encode($img) . ")", $one ['content']);
                                }
                            }
                        }
                    }
                }
                break;
            case 'preview':
            case 'pdf':
                // do nothing
                break;
        }

        $attr = array();
        if ($type == 'pdf') {

            $am = D('Member', 'Service');
            $am->init($one['uid']);
            $attr['realname'] = $am->get('profile.realname');
            $attr['doc_header_image'] = $am->get('extra_info.doc_header_image');
            if (empty($attr['doc_header_image'])) {
                $attr['doc_header_image'] = tpx_config_get('docmz_pdf_logo', 'asserts/res/img/pdf_logo.png');
            }

        }

        $this->data_attr = $attr;
        $this->data_one = $one;
        $this->display('R:' . $type_list[$type]);

    }

}